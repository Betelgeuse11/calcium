module gitlab.com/Betelgeuse11/calcium

go 1.18

require (
	github.com/julienschmidt/httprouter v1.3.0
	github.com/smacker/go-tree-sitter v0.0.0-20221023091341-2009a4db91e4
)
