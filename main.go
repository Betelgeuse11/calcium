package main

import (
	"log"

	"gitlab.com/Betelgeuse11/calcium/pkg/highlight"
)

func main() {
	hl := highlight.NewHighlightLanguage()

	parser, err := hl.ParserFor("golang")
	if err != nil {
		log.Fatal(err)
	}

	parser.ParseCode(`
func main() {
	return 0
}`)
}
