package highlight

import (
	"context"

	sitter "github.com/smacker/go-tree-sitter"
)

// Parser is a go-tree-sitter.Parser wrapper
type Parser struct {
	Language string
	parser   *sitter.Parser
}

func (p *Parser) ParseCode(code string) (*Tree, error) {
	data := []byte(code)
	tsTree, err := p.parser.ParseCtx(context.Background(), nil, data)
	if err != nil {
		return nil, err
	}

	tree := &Tree{
		Input: code,
	}
	head := tsTree.RootNode()
	tree.fillFromHead(head)

	return tree, nil
}

func (p *Parser) ParseFile(filename string) *Tree {
	return nil
}
