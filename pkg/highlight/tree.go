package highlight

import (
	"fmt"

	sitter "github.com/smacker/go-tree-sitter"
)

type Tree struct {
	Input string
	head  node
}

type node struct {
	children  []node
	colorized bool
	content   string
	nodetype  string
}

func (t *Tree) fillFromHead(tsHead *sitter.Node) {
	var (
		current *sitter.Node
		err error
	)
	t.head = t.parseTSNode(tsHead)

	iterator := sitter.NewIterator(tsHead, sitter.DFSMode)
	for {
		current, err = iterator.Next()
		fmt.Println(current, err)
		break
	}
}

func (t *Tree) parseTSNode(tsn *sitter.Node) node {
	content := t.Input[tsn.StartByte():tsn.EndByte()]

	return node{
		children: make([]node, 0),
		content:  content,
		nodetype: tsn.Type(),
	}
}
