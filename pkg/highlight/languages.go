package highlight

import (
	"fmt"

	sitter "github.com/smacker/go-tree-sitter"
	"github.com/smacker/go-tree-sitter/bash"
	"github.com/smacker/go-tree-sitter/c"
	"github.com/smacker/go-tree-sitter/cpp"
	"github.com/smacker/go-tree-sitter/csharp"
	"github.com/smacker/go-tree-sitter/css"
	"github.com/smacker/go-tree-sitter/cue"
	"github.com/smacker/go-tree-sitter/dockerfile"
	"github.com/smacker/go-tree-sitter/elixir"
	"github.com/smacker/go-tree-sitter/elm"
	"github.com/smacker/go-tree-sitter/golang"
	"github.com/smacker/go-tree-sitter/html"
	"github.com/smacker/go-tree-sitter/java"
	"github.com/smacker/go-tree-sitter/javascript"
	"github.com/smacker/go-tree-sitter/lua"
	"github.com/smacker/go-tree-sitter/ocaml"
	"github.com/smacker/go-tree-sitter/php"
	"github.com/smacker/go-tree-sitter/python"
	"github.com/smacker/go-tree-sitter/ruby"
	"github.com/smacker/go-tree-sitter/rust"
	"github.com/smacker/go-tree-sitter/scala"
	"github.com/smacker/go-tree-sitter/toml"
	"github.com/smacker/go-tree-sitter/typescript/typescript"
	"github.com/smacker/go-tree-sitter/yaml"
)

type highlightLanguages struct {
	Available []string
	TSLanguages map[string]*sitter.Language
}

func NewHighlightLanguage() highlightLanguages {
	languages := getTSLanguages()
	return highlightLanguages{
		TSLanguages: languages,
		Available: getAvailableLanguages(languages),
	}

}

func (hl *highlightLanguages) ParserFor(language string) (*Parser, error) {
	tsLanguage, exists := hl.TSLanguages[language]
	if !exists {
		// FIX: asking for go won't give you golang, which is an issue in my book.
		// Need a fix in that sense
		return nil, fmt.Errorf("Language %s isn't available", language)
	}

	parser := sitter.NewParser()
	parser.SetLanguage(tsLanguage)

	return &Parser{
		Language: language,
		parser: parser,
	}, nil
}

func getTSLanguages() map[string]*sitter.Language {
	return map[string]*sitter.Language{
		"bash":       bash.GetLanguage(),
		"c":          c.GetLanguage(),
		"cpp":        cpp.GetLanguage(),
		"csharp":     csharp.GetLanguage(),
		"css":        css.GetLanguage(),
		"cue":        cue.GetLanguage(),
		"dockerfile": dockerfile.GetLanguage(),
		"elixir":     elixir.GetLanguage(),
		"elm":        elm.GetLanguage(),
		"golang":     golang.GetLanguage(),
		"html":       html.GetLanguage(),
		"java":       java.GetLanguage(),
		"javascript": javascript.GetLanguage(),
		"lua":        lua.GetLanguage(),
		"ocaml":      ocaml.GetLanguage(),
		"php":        php.GetLanguage(),
		"python":     python.GetLanguage(),
		"ruby":       ruby.GetLanguage(),
		"rust":       rust.GetLanguage(),
		"scala":      scala.GetLanguage(),
		"toml":       toml.GetLanguage(),
		"typescript": typescript.GetLanguage(),
		"yaml":       yaml.GetLanguage(),
	}
}

func getAvailableLanguages(languages map[string]*sitter.Language) []string {
	available := make([]string, 0, len(languages))
	for language := range languages {
		available = append(available, language)
	}
	return available
}
